$( document ).ready(function() {
  $('.acreditaciones-carreras').slick({
    slidesToShow: 5,
    dots: false,
    responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 550,

      settings: {
        slidesToShow: 1,
        infinite: true,
      }
    }
    ]
  });
});
